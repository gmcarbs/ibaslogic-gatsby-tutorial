import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import {siteFooter, container} from "./footer.module.scss"

const Footer = () => {
    const data = useStaticQuery(
			graphql`
				query {
					site {
						siteMetadata {
							author
							description
							title
						}
					}
				}`
			)
      
    return (
			<footer className={siteFooter}>
				<div className={container}>
					<p>
						Site developed by {data.site.siteMetadata.author} &copy;{" "} 
						{new Date().getFullYear().toString()}{" "}
					</p>
				</div>
			</footer>
    )
}

export default Footer