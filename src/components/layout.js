import React from "react"
import Footer from "./footer"
import Header from "./header"
import "../styles/style.scss"
import { container, content, mainContent } from "./layout.module.scss"

const Layout = props => {
  return (
    <div className={container}>
      <div className={content}>
        <Header />
        <div className={mainContent}>{props.children}</div>
      </div>
      <Footer/>
    </div>
  )
}

export default Layout