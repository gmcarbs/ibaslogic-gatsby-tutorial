import { Link, useStaticQuery, graphql } from "gatsby"
import React from "react"

import { header, heroContent, brand, description, navContainer, navList, overlay, activeMenuItem } from "./header.module.scss"

const Header = () => {
  const data = useStaticQuery(
    graphql`
    query {
      site {
        siteMetadata {
          author
          description
          title
        }
      }
    }`
  )

  return(
    <header className={header}>
      <div className={overlay}>
        <div className={heroContent}>
          <p className={brand}>
            <Link to="/">{data.site.siteMetadata.title}</Link>
          </p>
          <p className={description}>
          {data.site.siteMetadata.description}
          </p>
        </div>
        <nav className={navContainer}>
          <ul className={navList}>
            <li>
              <Link to="/" activeClassName={activeMenuItem}>Home</Link>
            </li>
            <li>
              <Link to="/blog/" activeClassName={activeMenuItem}>Blog</Link>
            </li>
            <li>
              <Link to="/contact/" activeClassName={activeMenuItem}>Contact</Link>
            </li>
            <li>
              <Link to="/about/" activeClassName={activeMenuItem}>About</Link>
            </li>
          </ul>
        </nav>
      </div>
    </header>
  )
}

export default Header