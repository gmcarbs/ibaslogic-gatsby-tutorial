import React from "react"

import { useStaticQuery, graphql, Link } from "gatsby"
import Img from "gatsby-image"

import Layout from "../components/layout"
import Metadata from "../components/metadata"

import { posts, post, meta, featured, excerpt, button } from "./blog.module.scss"

const Blog = () => {
  const data = useStaticQuery(
    graphql`
      query MyQuery {
        allMarkdownRemark(sort: {fields: frontmatter___date, order: DESC}) {
          edges {
            node {
              excerpt
              frontmatter {
                date(formatString: "DD MMMM, YYYY")
                title
                featured {
                  childImageSharp {
                    fluid(maxWidth: 750) {
                      ...GatsbyImageSharpFluid
                    }
                  }
                }
              }
              timeToRead
              id
              fields {
                slug
              }
            }
          }
        }
      }
    `
  )
  return (
    <Layout>
      <Metadata title="Blog" description="Blog contents" />
      <ul className={posts}>
        {
          data.allMarkdownRemark.edges.map(edge => {
            return (
              <li className={post} key={edge.node.id}>
                <h2>
                  <Link to={`/blog/${edge.node.fields.slug}/`}>
                    {edge.node.frontmatter.title}
                  </Link>
                </h2>
                <div className={meta}>
                  <span>
                    Posted on {edge.node.frontmatter.date} <span> / </span>{" "}
                    {edge.node.timeToRead} min read
                  </span>
                </div>
                {
                  edge.node.frontmatter.featured && (
                    <Img
                      className={featured}
                      fluid={edge.node.frontmatter.featured.childImageSharp.fluid} 
                      alt={edge.node.frontmatter.title}
                    />
                  )
                }
                <p className={excerpt}>{edge.node.excerpt}</p>
                <div className={button}>
                  <Link to={`/blog/${edge.node.fields.slug}/`}>Read More</Link>
                </div>
              </li>
            )
          })
        }
      </ul>
    </Layout>
  )
}

export default Blog