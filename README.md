Plugins
* gatsby-remark-images:       use images in markdown post, image optimizations
* gatsby-plugin-sharp:        reducing image size
* gatsby-image:               render optimized image(fixed and full-width images)
* gatsby-transformer-sharp:   expose node 'childImageSharp' for image processing

Sample change to test commit signing